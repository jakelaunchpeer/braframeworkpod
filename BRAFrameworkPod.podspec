#
# Be sure to run `pod lib lint BRAFrameworkPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BRAFrameworkPod'
  s.version          = '0.1.0'
  s.summary          = 'BRA Framework interacts with Parse Server'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "This is a nice decsription"

  s.homepage         = 'https://github.com/<GITHUB_USERNAME>/BRAFrameworkPod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Timothy Barrett' => 'liltimtim@gmail.com' }
  s.source           = { :git => 'https://github.com/<GITHUB_USERNAME>/BRAFrameworkPod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'BRAFrameworkPod/Classes/**/*'
  
  # s.resource_bundles = {
  #   'BRAFrameworkPod' => ['BRAFrameworkPod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
    s.dependency 'Parse'
    s.dependency 'Bolts'
    s.dependency 'Bolts-Swift'
end
