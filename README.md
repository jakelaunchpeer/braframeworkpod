# BRAFrameworkPod

[![CI Status](http://img.shields.io/travis/Timothy Barrett/BRAFrameworkPod.svg?style=flat)](https://travis-ci.org/Timothy Barrett/BRAFrameworkPod)
[![Version](https://img.shields.io/cocoapods/v/BRAFrameworkPod.svg?style=flat)](http://cocoapods.org/pods/BRAFrameworkPod)
[![License](https://img.shields.io/cocoapods/l/BRAFrameworkPod.svg?style=flat)](http://cocoapods.org/pods/BRAFrameworkPod)
[![Platform](https://img.shields.io/cocoapods/p/BRAFrameworkPod.svg?style=flat)](http://cocoapods.org/pods/BRAFrameworkPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BRAFrameworkPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "BRAFrameworkPod"
```

## Author

Timothy Barrett, liltimtim@gmail.com

## License

BRAFrameworkPod is available under the MIT license. See the LICENSE file for more info.
