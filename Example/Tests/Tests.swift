//
//  BRAFrameworkTests.swift
//  BRAFrameworkTests
//
//  Created by Timothy Barrett on 7/30/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

import XCTest
import Parse
import ParseLiveQuery
@testable import BRAFrameworkPod

class ParsyTests:XCTestCase {
    
    override func setUp() {
        Product.registerSubclass()
        Request.registerSubclass()
        Charge.registerSubclass()
        let envVars = NSProcessInfo.processInfo().environment
        if Parse.currentConfiguration() == nil {
            if envVars["local_dev"] != nil {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "http://127.0.0.1:1337/parse"
                }))
            } else {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "https://braprod.herokuapp.com/parse"
                }))
            }
        }
    }
    
    func testSubscriptionServiceForRequest() {
        let _ = expectationForNotification("requestedUpdated", object: nil, handler: nil)
        let request = Request(className: Request.parseClassName())
        request.saveInBackgroundWithBlock { (success, error) in
            XCTAssertTrue(success)
            XCTAssertNil(error)
            request.delegate = self
            request.subscribe()
            let currentTime = NSDate()
            request.pickupTime = currentTime
            request.saveInBackgroundWithBlock({ (success, error) in
                XCTAssertTrue(success)
                XCTAssertNil(error)
                XCTAssertEqual(currentTime, request.pickupTime)
            })
        }
        waitForExpectationsWithTimeout(30, handler: nil)
    }
    
}

extension ParsyTests : RequestDelegate {
    func requestUpdated(request: Request) {
        print(request)
        NSNotificationCenter.defaultCenter().postNotificationName("requestedUpdated", object: nil)
    }
    func requestError(error: NSError?) {
        
    }
}

class PFInterfaceTests:XCTestCase {
    private let username = "liltimtim"
    private let password = "password1"
    override func setUp() {
        Product.registerSubclass()
        Request.registerSubclass()
        Charge.registerSubclass()
        Document.registerSubclass()
        let envVars = NSProcessInfo.processInfo().environment
        if Parse.currentConfiguration() == nil {
            if envVars["local_dev"] != nil {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "http://127.0.0.1:1337/parse"
                }))
            } else {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "https://braprod.herokuapp.com/parse"
                }))
            }

        }
    }
    
    func testFindNearestLocationToCurrentLocation() {
        let exp = expectationWithDescription("Find location nearest poinst")
        PFInterface.findClosestPickupPoint { (location, error) in
            XCTAssertNotNil(location)
            XCTAssertNil(error)
            exp.fulfill()
        }
        waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testFindNearestLocationToPoint() {
        let exp = expectationWithDescription("Find nearest point to a given location")
        PFInterface.findClosestPickup(forPoint: PFGeoPoint(latitude: 32.77527, longitude: -79.924519)) { (location, error) in
            XCTAssertNotNil(location)
            print(location)
            print(location?.name)
            XCTAssertNil(error)
            exp.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testPickupRequest() {
        let exp = expectationWithDescription("Pickup a request as captain")
        PFInterface.authenticate(withUsername: username, withPassword: password) { (user, error) in
            XCTAssertNotNil(user)
            XCTAssertNil(error)
            
            print(user)
            let request = Request(className: Request.parseClassName())
            request.saveInBackgroundWithBlock({ (success, error) in
                XCTAssertTrue(success)
                XCTAssertNil(error)
                PFInterface.pickupRequest(PFUser.currentUser()!.objectId!, request: request, completion: { (request, error) in
                    XCTAssertNotNil(request)
                    XCTAssertNil(error)
                    exp.fulfill()
                })
            })
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testDropOffRequest() {
        let exp = expectationWithDescription("Pickup a request as captain")
        PFInterface.authenticate(withUsername: username, withPassword: password) { (user, error) in
            XCTAssertNotNil(user)
            XCTAssertNil(error)
            
            print(user)
            let request = Request(className: Request.parseClassName())
            request.requester = PFUser.currentUser()
            request.captain = PFUser.currentUser()
            request.pickupTime = NSDate()
            request.cancelled = false
            PFInterface.getAllProducts({ (products, error) in
                XCTAssertGreaterThan(products.count, 0)
                request.product = products.first
                request.saveInBackgroundWithBlock({ (success, error) in
                    XCTAssertTrue(success)
                    XCTAssertNil(error)
                    PFInterface.dropoffRequest(PFUser.currentUser()!.objectId!, passengerUserId: PFUser.currentUser()!.objectId!, request: request, completion: { (request, error) in
                        XCTAssertNil(error)
                        XCTAssertNotNil(request?.chargeAmount)
                        XCTAssertNotNil(request?.charge)
                        XCTAssertNotNil(request?.captain)
                        XCTAssertNotNil(request?.requester)
                        exp.fulfill()
                    })
                })
            })
            
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testAddPaymentMethod() {
        let exp = expectationWithDescription("add payment method")
        PFInterface.authenticate(withUsername: "liltimtim", withPassword: "password1") { (user, error) in
            XCTAssertNotNil(user)
            XCTAssertNil(error)
            XCTAssertNotNil(user?.objectId)
            PFInterface.addPaymentMethod(user!.objectId!, cardNumber: "4242424242424242", cvc: "345", expMonth: 10, expYear: 2020, completion: { error in
                print(error)
                XCTAssertNil(error)
                exp.fulfill()
            })
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testAddPaymentInvalidCardNumber() {
        let exp = expectationWithDescription("add payment method")
        PFInterface.authenticate(withUsername: "liltimtim", withPassword: "password1") { (user, error) in
            XCTAssertNotNil(user)
            XCTAssertNil(error)
            XCTAssertNotNil(user?.objectId)
            PFInterface.addPaymentMethod(user!.objectId!, cardNumber: "424242424242424", cvc: "345", expMonth: 10, expYear: 2020, completion: { error in
                XCTAssertNotNil(error)
                exp.fulfill()
            })
        }
        waitForExpectationsWithTimeout(25, handler: nil)
    }
    
    func testAddPaymentInvalidUser() {
        let exp = expectationWithDescription("add payment method")
        PFInterface.authenticate(withUsername: "liltimtim", withPassword: "password1") { (user, error) in
            XCTAssertNotNil(user)
            XCTAssertNil(error)
            XCTAssertNotNil(user?.objectId)
            PFInterface.addPaymentMethod("123125", cardNumber: "424242424242424", cvc: "345", expMonth: 10, expYear: 2020, completion: { error in
                XCTAssertNotNil(error)
                print(error)
                exp.fulfill()
            })
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testAuthenticateUser() {
        PFInterface.authenticate(withUsername: "liltimtim", withPassword: "password1") { (user, error) in
            XCTAssertNotNil(user)
            XCTAssertNil(error)
        }
    }
    
    func testGetAllRequests() {
        let exp = expectationWithDescription("inflate parse objects from cloud code")
        PFInterface.getAllRequests { (requests, error) in
            XCTAssertNil(error)
            XCTAssertGreaterThan(requests.count, 0)
            for request in requests {
                print(request.pickupPoint)
                print(request.chargeCompleted)
                print(request.chargeAmount)
                print(request.captain)
                print(request.requester)
                print(request)
            }
            exp.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testGetAllProducts() {
        let exp = expectationWithDescription("Get All products")
        PFInterface.getAllProducts { (products, error) in
            XCTAssertNil(error)
            XCTAssertGreaterThan(products.count, 0)
            for product in products {
                print(product.title)
            }
            exp.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
}

class DocumentTests:XCTestCase {
    
    override func setUp() {
        Product.registerSubclass()
        Request.registerSubclass()
        Charge.registerSubclass()
        Document.registerSubclass()
        let envVars = NSProcessInfo.processInfo().environment
        if Parse.currentConfiguration() == nil {
            //            if envVars["local_dev"] != nil {
            //
            //            } else {
            //                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
            //                    configs.applicationId = "bra"
            //                    configs.server = "https://braprod.herokuapp.com/parse"
            //                }))
            //            }
            Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                configs.applicationId = "bra"
                configs.server = "http://127.0.0.1:1337/parse"
            }))
        }
    }
    /*
    func testCreateDocument() {
        let exp = expectationWithDescription("Create and save a document to server")
        let document = Document(className: Document.parseClassName())
        
        document.file = PFFile(data: UIImageJPEGRepresentation(UIImage(), 1.0)!, contentType: "jpeg")
        document.saveInBackgroundWithBlock { (success, error) in
            XCTAssertTrue(success)
            exp.fulfill()
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
 */
}

class SubscriptionServiceTests:XCTestCase {
    private let username:String = "liltimtim"
    private let password:String = "password1"
    
    override func setUp() {
        Product.registerSubclass()
        Request.registerSubclass()
        let envVars = NSProcessInfo.processInfo().environment
        if Parse.currentConfiguration() == nil {
            if envVars["local_dev"] != nil {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "http://192.168.1.148:1337/parse"
                }))
            } else {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "https://braprod.herokuapp.com/parse"
                }))
            }
        }
    }
    
    func testSubscriptionRequestUpdated() {
        let subscriptionManager = SubscriptionService()
        subscriptionManager.delegate = self
        let _ = expectationForNotification("requestUpdated", object: nil) { (note) -> Bool in
            if let request = note.object as? [Request] {
                print(request)
            } else {
                XCTFail("Notification posted without a request object")
            }
            return true
        }
        let request = Request(className: Request.parseClassName())
        
        request.cancelled = false
        request.pickupPoint = PFGeoPoint(location: CLLocation(latitude: 1.0001, longitude: 1.0))
        
        PFInterface.authenticate(withUsername: username, withPassword: password) { (user, error) in
            XCTAssertNotNil(user)
            request.requester = PFUser.currentUser()
            request.saveInBackgroundWithBlock({ (success, error) in
                XCTAssertTrue(success)
                XCTAssertNil(error)
                
                let point = PFGeoPoint(latitude: 1.0, longitude: 1.0)
                subscriptionManager.beginUpdatingRequests()
                request.cancelled = false
                request.pickupPoint = point
                request.saveInBackgroundWithBlock({ (success, error) in
                    print("Request saved with updated point")
                    XCTAssertTrue(success)
                    XCTAssertNil(error)
                })
            })
        }
        waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    func testIfUserCanMakeRequests() {
        let exp = expectationWithDescription("test to see if the user can make a request and has a card on file")
        PFInterface.authenticate(withUsername: username, withPassword: password) { (user, error) in
            XCTAssertNotNil(user)
            PFInterface.userCanMakeRequests(withUser: user, completion: { (enabled) in
                XCTAssertTrue(enabled)
                exp.fulfill()
            })
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testMakeRequestNoCreditCard() {
        let exp = expectationWithDescription("attempt to make a request without a credit card")
        PFInterface.authenticate(withUsername: "liltimtim1", withPassword: "password1") { (user, error) in
            XCTAssertNotNil(user)
            PFInterface.userCanMakeRequests(withUser: user, completion: { (enabled) in
                XCTAssertFalse(enabled)
                let request = Request(className: Request.parseClassName())
                PFInterface.createRequest(forUser: user!, withRequest: request, completion: { (success, error) in
                    XCTAssertFalse(success)
                    print(error)
                    XCTAssertNotNil(error)
                    exp.fulfill()
                })
            })
        }
        waitForExpectationsWithTimeout(10, handler: nil)
    }
}
extension SubscriptionServiceTests : SubscriptionServiceDelegate {
    func requestUpdated(requests: [Request]) {
        NSNotificationCenter.defaultCenter().postNotificationName("requestUpdated", object: requests)
    }
    
    func requestCreated(request: Request?) {
        NSNotificationCenter.defaultCenter().postNotificationName("requestCreated", object: request)
    }
}

class LocationTrackingManagerTests:XCTestCase {
    private let username:String = "liltimtim"
    private let password:String = "password1"
    override func setUp() {
        Product.registerSubclass()
        Request.registerSubclass()
        Charge.registerSubclass()
        LocationTracking.registerSubclass()
        let envVars = NSProcessInfo.processInfo().environment
        if Parse.currentConfiguration() == nil {
            if envVars["local_dev"] != nil {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "http://192.168.1.148:1337/parse"
                }))
            } else {
                Parse.initializeWithConfiguration(ParseClientConfiguration(block: { (configs) in
                    configs.applicationId = "bra"
                    configs.server = "https://braprod.herokuapp.com/parse"
                }))
            }
        }
    }
    func testSubscriptionUpdatingLocation() {
        let _ = expectationForNotification("locationUpdated", object: nil, handler: nil)
        let location = LocationTracking(className: LocationTracking.parseClassName())
        PFInterface.authenticate(withUsername: username, withPassword: password) { (user, error) in
            XCTAssertNotNil(user)
            XCTAssertNil(error)
            location.location = PFGeoPoint(latitude: 1.0, longitude: 1.0)
            let newLocation = LocationTracking(className: LocationTracking.parseClassName())
            newLocation.user = PFUser.currentUser()
            newLocation.location = PFGeoPoint(latitude: 1.2, longitude: 1.0)
            newLocation.saveInBackgroundWithBlock({ (success, error) in
                XCTAssertTrue(success)
                XCTAssertNil(error)
                XCTAssertNil(error)
                let locationTracker = LocationTrackingManager(begingTrackingUser: PFUser.currentUser()!)
                locationTracker.delegate = self
                location.location = PFGeoPoint(latitude: 1.2, longitude: 1.0)
            })
        }
        
        waitForExpectationsWithTimeout(30, handler: nil)
    }
}

extension LocationTrackingManagerTests: LocationTrackingManagerDelegate {
    func userLocationUpdated(toNewLocation: LocationTracking?) {
        XCTAssertNotNil(toNewLocation)
        XCTAssertEqual(toNewLocation?.location?.latitude, 1.2)
        NSNotificationCenter.defaultCenter().postNotificationName("locationUpdated", object: toNewLocation)
    }
    
    func userLocationThrewError(error: NSError?) {
        NSNotificationCenter.defaultCenter().postNotificationName("locationThrewError", object: error)
    }
}