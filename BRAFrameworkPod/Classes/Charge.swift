//
//  Charge.swift
//  BRAFramework
//
//  Created by Timothy Barrett on 7/31/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

import Foundation
import Parse
public class Charge: PFObject, PFSubclassing {
    @NSManaged var product:Product?
    @NSManaged var passenger:PFUser?
    @NSManaged var captain:PFUser?
    
    public func getTotalCharge() -> Double? {
        return self["totalCharge"] as? Double
    }
    
    public static func parseClassName() -> String {
        return "Charge"
    }
    
}