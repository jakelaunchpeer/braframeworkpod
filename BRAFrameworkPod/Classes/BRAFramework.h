//
//  BRAFramework.h
//  BRAFramework
//
//  Created by Timothy Barrett on 7/30/16.
//  Copyright © 2016 BRA LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BRAFramework.
FOUNDATION_EXPORT double BRAFrameworkVersionNumber;

//! Project version string for BRAFramework.
FOUNDATION_EXPORT const unsigned char BRAFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BRAFramework/PublicHeader.h>


