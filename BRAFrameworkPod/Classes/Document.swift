//
//  Document.swift
//  Pods
//
//  Created by Timothy Barrett on 8/7/16.
//
//

import Foundation
import Parse
public class Document : PFObject, PFSubclassing {
    @NSManaged var file:PFFile?
    @NSManaged var owner:PFUser?
    @NSManaged var contentType:String?
    
    public struct ContentType {
        static let Document = "Document"
        static let Image = "Image"
    }
    public static func parseClassName() -> String {
        return "Document"
    }
}