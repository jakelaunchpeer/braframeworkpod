//
//  File.swift
//  Pods
//
//  Created by Timothy Barrett on 8/8/16.
//
//

import Foundation
import Parse
public class Location : PFObject, PFSubclassing {
    @NSManaged public var name:String?
    @NSManaged public var point:PFGeoPoint?
    @NSManaged public var address:String?
    
    public static func parseClassName() -> String {
        return "Location"
    }
}